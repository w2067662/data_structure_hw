#include <iostream>
using namespace std;

typedef enum desertstate {
    wither = 0,
    oasis = 1,
};

template <typename T>
class vector {
//   typedef T* iterator;
private:
    T* arr;
    int capacity;
    int current;
public:
    vector() {
        arr = new T[1];
        capacity = 1;
        current = 0;
    }
    vector(int size) {
        arr = new T[size];
        capacity = size;
        current = 0;
    }
    ~vector() {
        delete[] arr;
    }
    void resize(int size) {
        arr = new T[size];
        capacity = size;
        current = 0;
    }
    void push_back(T data) {
        if (current == capacity) {
            T* temp = new T[2 * capacity];
            for (int i = 0; i < capacity; i++) {
                temp[i] = arr[i];
            }

            delete[] arr;
            capacity *= 2;
            arr = temp;
        }

        arr[current] = data;
        current++;
    }
    void push(T data, int index) {
        if (index == capacity) push(data);

        else arr[index] = data;
    }
    T get(int index) {
        if (index < current && index >= 0)return arr[index];
    }
    T& operator[] (int index) {
        if (index < current && index >= 0)return arr[index];
    }
    vector<T> operator[] (int index) const {
        if (index < current && index >= 0)return arr[index];
    }

    void pop() { current--; }
    int size() { return current; }
    int getcapacity() { return capacity; }

    void print() {
        for (int i = 0; i < current; i++) {
            if (i == current - 1)cout << arr[i];
            else cout << arr[i] << " ";
        }
        cout << endl;
    }

//    typename iterator begin() { return arr; }
//    typename iterator end() { return arr + size(); }
    T& front(void) { return arr[0]; }
    T& back() { return arr[current - 1]; }
};

template<class T>
class Queue {
private:
    int capacity;
    int front;
    int back;
    T* queue;
public:
    Queue() :capacity(5), front(0), back(0) {
        queue = new T[capacity];
    }
    void doubleCapacity() {
        T* newQueue = new T[capacity * 2];

        int j = front, temp_size = size();
        for (int i = 1; i <= temp_size; i++) {
            newQueue[i] = queue[++j % capacity];
        }

        back = size();
        front = 0;
        capacity *= 2;

        delete[] queue;
        queue = newQueue;
    }
    void push(T pos) {
        if (full()) doubleCapacity();
        back = (back + 1) % capacity;
        queue[back] = pos;
    }
    void pop() {
        if (empty())  return;
        front = (front + 1) % capacity;
    }
    bool empty() {
        return (front == back);
    }
    bool full() {
        return ((back + 1) % capacity == front);
    }
    T getFront() {
        if (empty())  return -1;
        return queue[(front + 1) % capacity];
    }
    T getBack() {
        if (empty()) return -1;
        return queue[back];
    }
    int size() {
        int size;
        if (front < back)   size = back - front;
        else   size = capacity - (front - back);
        return size;
    }
    void print(void) {
        cout << "elements in queue:";
        for (int i = 0; i < size(); i++) {
            cout << queue[(front + 1 + i) % capacity] << " ";
        }
        cout << endl;
    }
};

template<typename T>
class Stack {
private:
    T* data;
    int Size;
    int Top;
    const int Buttom = 0;
public:
    Stack() : Size(5), Top(-1) {
        data = new T[Size];
    }
    Stack(int size) : Size(size), Top(-1) {
        data = new T[Size];
    }
    void doubleSize() {
        int new_size = Size * 2;
        T* new_data = new T[new_size];

        for (int i = 0; i < Size; i++) {
            new_data[i] = data[i];
        }
        //delete[] data;
        this->data = new_data;
        this->Size = new_size;
        
    }
    bool push(T const& x) {
        if (full())doubleSize();
        int temp = Top + 1;
        data[temp] = x;
        this->Top = temp;
        return true;
    }
    bool empty() {
        return Top == -1;
    }
    T const& top() const {
        return data[Top];
    }
    bool pop() {
        if (empty())return false;
        this->Top -= 1;
        return true;
    }
    bool full() {
        return Top + 1 >= Size;
    }
    int size() {
        return Top + 1;
    }
    void print(void) {
        cout << "elements in stack:";
        for (int i = 0; i < size(); i++) {
            cout << data[i] << " ";
        }
        cout << endl;
    }
};

class graph {
private:
    desertstate* states;
    vector<int>* adjacency_list;
    int nodesAmount;
public:
    graph() {
        nodesAmount = 0;
        adjacency_list = new vector<int>[nodesAmount];
    }
    graph(int vertices) {
        nodesAmount = vertices;
        adjacency_list = new vector<int>[vertices];
    }
    void resize(int size) {
        nodesAmount = size;
        adjacency_list = new vector<int>[size];
    }

    void setStates(desertstate* states) { this->states = states; }

    void insertEdge(int u, int v) {
        adjacency_list[u].push_back(v);
        adjacency_list[v].push_back(u);

    }
    void printGraph() {
        for (int j = 0; j < nodesAmount; j++) {
            cout << "node of index " << j << ": " << j;
            for(int i=0;i< adjacency_list[j].size();i++){
                int it = adjacency_list[j][i];
//            for (const auto& it : adjacency_list[j]) {
                cout << "->" << it;
            }
            cout << endl;
        }
    }

    bool movable(int start, int end) {
        Stack<int> stack;
        bool* visited = new bool[nodesAmount];
        for (int i = 0; i < nodesAmount; i++) visited[i] = false;
        return DFS(start, end, 0, visited, stack);
    }

    //bool BFS(int start, int end){
    //    bool* visited = new bool[nodesAmount];
    //    for (int i = 0; i < nodesAmount; i++)visited[i] = false;
    //    Queue<int> queue;

    //    visited[start] = true;
    //    queue.push(start);
    //    int continuous_wither_node = 0;

    //    while (!queue.empty()){
    //        queue.print();
    //        int next = queue.getFront();

    //        if (states[next] == wither) continuous_wither_node++;
    //        else if (states[next] == oasis)continuous_wither_node = 0;
    //        cout << "Front:" << next << "  continuous_wither_node:" << continuous_wither_node << endl;
    //        cout << next << " state:" << states[next]<< endl;
    //        

    //        if (continuous_wither_node >= 2) {
    //            continuous_wither_node--;
    //            queue.pop();
    //            continue;
    //        }

    //        if (next == end)return true;

    //        for (const auto& it : adjacency_list[next]) {
    //            if (!visited[it]) {
    //                visited[it] = true;
    //                queue.push(it);
    //            }
    //        }
    //        queue.pop();
    //    }
    //    return false;
    //}

    bool DFS(int start, int end , int continuous_wither_node, bool* visited, Stack<int> stack) {
        stack.push(start);
        visited[start] = true;

        if (states[start] == wither)continuous_wither_node++;
        else if (states[start] == oasis)continuous_wither_node = 0;

        //stack.print();
        //cout << start << "state:" << states[start] << "  continuous_wither_node:" << continuous_wither_node << endl;

        if (continuous_wither_node >= 2) {
            visited[stack.top()] = false;
            stack.pop();
            visited[stack.top()] = false;
            stack.pop();
            return false;
        }
        if (start == end) return true;


        bool ans = false;
        for(int i=0;i< adjacency_list[start].size();i++){
            int it = adjacency_list[start][i];
//        for (const auto& it : adjacency_list[start]) {
            if (!visited[it]) {
                if(DFS(it, end, continuous_wither_node, visited, stack))ans = true;
            }
        } 
        return ans;
    }
};


int main() {
    graph graph;

    int N, K;
    cin >> N; cin >> K;

    graph.resize(N);
    desertstate* states = new desertstate[N];
    for (int i = 0; i < N; i++) {
        int state;
        cin >> state;
        if (state == 0) states[i] = wither;
        else if (state == 1)states[i] = oasis;
    }
    graph.setStates(states);

    for (int i = 0; i < K; i++){
        int u, v;
        cin >> u;  cin >> v;
        graph.insertEdge(u, v);
    }
    //graph.printGraph();

    int start = 0, end = 0;
    while (cin >> start) {
        cin >> end;
        //graph.movable(start, end) ? cout << "Yes" << endl : cout << "No" << endl;
        graph.movable(start, end) ? cout << "Yes" << endl : cout << "No" << endl;
    }
}
