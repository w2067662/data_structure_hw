#include <iostream>
using namespace std;


template <typename T>
class container {
    T* arr;
    int capacity;
    int current;

public:
    container() {
        arr = new T[1];
        capacity = 1;
        current = 0;
    }

    container(int size) {
        arr = new T[size];
        capacity = size;
        current = 0;
    }

    void push(T data) {
        if (current == capacity) {
            T* temp = new T[2 * capacity];
            for (int i = 0; i < capacity; i++) {
                temp[i] = arr[i];
            }

            delete[] arr;
            capacity *= 2;
            arr = temp;
        }

        arr[current] = data;
        current++;
    }

    void push(T data, int index) {
        if (index == capacity) push(data);

        else arr[index] = data;
    }

    T get(int index) {
        if (index < current && index >= 0)return arr[index];
    }

    T& operator[] (int index) {
        if (index < current && index >= 0)return arr[index];
    }

    void pop() { current--; }

    int size() { return current; }

    int getcapacity() { return capacity; }

    void print() {
        for (int i = 0; i < current; i++) {
            if (i == current - 1)cout << arr[i];
            else cout << arr[i] << " ";
        }
        cout << endl;
    }

    int randomPartition(int left, int right) {
        srand(time(NULL));
        int random = left + rand() % (right - left);
        swap(arr[random], arr[left]);

        int key = arr[left];
        int i = left - 1, j = right + 1;
        while (1) {
            do { i++; }
            while (arr[i] < key);
            do { j--; } 
            while (arr[j] > key);

            if (i >= j) return j;
            swap(arr[i], arr[j]);
        }
        swap(arr[i + 1], arr[right]);
        return i + 1;
    }

    void quickSort(int left, int right) {
        if (left < right) {
            int key = randomPartition(left, right);
            quickSort(left, key);
            quickSort(key + 1, right);
        }
    }

    int binary_search(int start, int end, int key) {
        int ans = -1;

        int mid;
        while (start <= end) {
            mid = start + (end - start) / 2;
            if (arr[mid] < key)
                start = mid + 1;
            else if (arr[mid] > key)
                end = mid - 1;
            else {
                ans = mid;
                break;
            }
        }
        return ans;
    }
};



int main() {
	int N;
	cin >> N;

	while (N--) {
		int M;
        cin >> M;

        container<int> list;
		while (M--) {
			int number;
			cin >> number;

            list.push(number);
		}
        list.quickSort(0, list.size()-1);
        list.print();
	}
}
