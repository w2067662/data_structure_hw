#include<iostream>
#include<string>
using namespace std;

typedef pair<int, int> Pair;

struct Treenode {
    int val;
    Treenode* left;
    Treenode* right;
    Treenode() :val(0), left(NULL), right(NULL) {};
    Treenode(int x) :val(x), left(NULL), right(NULL) {};
    Treenode(int x, Treenode* L, Treenode* R) :val(x), left(L), right(R) {};
};

class BST {
public:
    Treenode* root;
    int node_counts;
    int sum;

public:
    BST() {
        root = NULL;
        node_counts = 0;
        sum = 0;
    }

    BST(Treenode* node) {
        root = node;
        node_counts = 0;
        sum = 0;
    }

    void insert(Treenode* root, int val) {
        Treenode* temp = new Treenode(val);
        Treenode* curr = root;
        Treenode* insert_node = NULL;

        while (curr) {
            insert_node = curr;
            if (val > curr->val)curr = curr->right;
            else curr = curr->left;
        }

        if (insert_node == NULL) insert_node = temp;
        else if (val > insert_node->val) insert_node->right = temp;
        else insert_node->left = temp;

        return;
    }

    Treenode* findMax(Treenode* root) {
        while (root->right)
            root = root->right;
        return root;
    }

    Treenode* deleteNode(Treenode* root, int key) {
        if (!root) return NULL;

        if (root->val > key) root->left = deleteNode(root->left, key);
        else if (root->val < key) root->right = deleteNode(root->right, key);
        else {
            if (!root->right && !root->left)
                delete root,
                root = NULL;
            else if (!root->left) {
                Treenode* node = root;
                root = root->right;
                delete node;
            }
            else if (!root->right) {
                Treenode* node = root;
                root = root->left;
                delete node;
            }
            else {
                Treenode* node = findMax(root->left);
                root->val = node->val;
                root->left = deleteNode(root->left, root->val);
            }
        }
        return root;
    }


    string print(Treenode* root, string str) {
        string res = "";
        string left_str = "";
        string right_str = "";

        if (!root)return res;
        else {
            res = to_string(root->val)
                + '(' + print(root->left, left_str) + ')'
                + '(' + print(root->right, right_str) + ')';
        }

        return res;
    }

    Pair removable_least_number(Treenode* root, int Vmax, Pair current) {
        if (!root)return current;
        else {
            current = removable_least_number(root->right, Vmax, current);

            if (current.first <= Vmax)return current;
            if (current.first > Vmax) {
                current.second++;
            }
            current.first -= root->val;

            //cout << root->val << endl;

            current = removable_least_number(root->left, Vmax, current);
        }
        return current;
    }

    Pair removable_most_number(Treenode* root, int Vmin, Pair current) {
        if (!root)return current;
        else {
            current = removable_most_number(root->left, Vmin, current);

            if (current.first - root->val < Vmin)return current;
            if (current.first - root->val >= Vmin) {
                current.second++;
            }
            current.first -= root->val;

            //cout << root->val << endl;

            current = removable_most_number(root->right, Vmin, current);
        }
        return current;
    }
};




int main() {
    int N;
    cin >> N;

    BST bst;


    //initial insertions of BST
    while (N--) {
        int value;
        cin >> value;

        if (!bst.root) {
            bst.root = new Treenode(value);
        }
        else  bst.insert(bst.root, value);
        bst.node_counts++;
        bst.sum += value;
    }


    char input = '\0';
    while (cin >> input) {
        switch (input) {
        case 'P': {
            string res = "";
            res = bst.print(bst.root, res);

            //cout << "Print:" << endl;
            cout << res << endl;
            break;
        }
        case 'I': {
            int value;
            cin >> value;

            if (!bst.root) {
                bst.root = new Treenode(value);
            }
            else bst.insert(bst.root, value);
            bst.node_counts++;
            bst.sum += value;
            //cout << "Insert" << endl;
            break;
        }
        case 'D': {
            int key_value;
            cin >> key_value;

            bst.deleteNode(bst.root, key_value);
            bst.node_counts--;
            bst.sum -= key_value;
            //cout << "Delete: " << value << endl;
            break;
        }
        case 'L': {
            int Vmax;
            cin >> Vmax;
            //cout << "Least number: " << value << endl;

            Pair initial = { bst.sum,0 };
            cout << bst.removable_least_number(bst.root, Vmax, initial).second << endl;
            break;
        }
        case 'M': {
            int Vmin;
            cin >> Vmin;
            //cout << "Most number: " << value << endl;

            Pair initial = { bst.sum,0 };
            cout << bst.removable_most_number(bst.root, Vmin, initial).second << endl;
            break;
        }

        default://default
            break;
        }



    }


}
