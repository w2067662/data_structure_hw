#include<iostream>
#include<string>
using namespace std;


struct Treenode {
    int val;
    int height;
    Treenode* left, * right, * parent;
    Treenode() : val(0), left(NULL), right(NULL), parent(NULL), height(0) {};
    Treenode(int val) : val(val), left(NULL), right(NULL), parent(NULL), height(0) {};
};

template<class T>
struct node {
    T obj;
    int height;
    node* left, * right, * parent;
    node() : obj(0), left(NULL), right(NULL), parent(NULL), height(0) {};
    node(T obj) : obj(obj), left(NULL), right(NULL), parent(NULL), height(0) {};
};


class AVL {
public:
    Treenode* root = NULL;
    int sum;

public:
    AVL() {
        root = NULL;
        sum = 0;
    }

    AVL(Treenode* Treenode) {
        root = Treenode;
        sum = 0;
    }

    int height(Treenode* root) {
        if (!root)return 0;
        int lh = (!root->left) ? root->left->height + 1 : 0;
        int rh = (!root->right) ? root->right->height + 1 : 0;

        if (lh > rh)return(lh);

        return(rh);
    }

    int balanceFactor(Treenode* root) {
        if (!root) return 0;
        int lh = (!root->left) ? root->left->height + 1 : 0;
        int rh = (!root->right) ? root->right->height + 1 : 0;

        return(lh - rh);
    }

    void updateHeight(Treenode* root) {
        if (!root)return;
        else {
            int val = 1;
            if (root->left) val = root->left->height + 1;
            if (root->right) val = max(val, root->right->height + 1);
            root->height = val;
        }
    }

    Treenode* LLrotation(Treenode* root) {
        Treenode* temp = root->left;
        root->left = temp->right;
        if (temp->right) temp->right->parent = root;

        temp->right = root;
        temp->parent = root->parent;

        root->parent = temp;

        if (temp->parent && root->val < temp->parent->val) temp->parent->left = temp;
        else {
            if (temp->parent) temp->parent->right = temp;
        }

        root = temp;

        updateHeight(root->left);
        updateHeight(root->right);
        updateHeight(root);
        updateHeight(root->parent);

        return root;
    }

    Treenode* RRrotation(Treenode* root) {
        Treenode* temp = root->right;
        root->right = temp->left;
        if (temp->left) temp->left->parent = root;
        temp->left = root;
        temp->parent = root->parent;
        root->parent = temp;

        if (temp->parent && root->val < temp->parent->val)temp->parent->left = temp;
        else {
            if (temp->parent) temp->parent->right = temp;
        }

        root = temp;

        updateHeight(root->left);
        updateHeight(root->right);
        updateHeight(root);
        updateHeight(root->parent);
        return root;
    }

    Treenode* LRrotation(Treenode* root) {
        root->left = RRrotation(root->left);
        return LLrotation(root);
    }

    Treenode* RLrotation(Treenode* root) {
        root->right = LLrotation(root->right);
        return RRrotation(root);
    }

    Treenode* Balance(Treenode* root) {
        int rootleftheight = 0;
        int rootrightheight = 0;

        if (root->left) rootleftheight = root->left->height;
        if (root->right) rootrightheight = root->right->height;

        if (abs(rootleftheight - rootrightheight) >= 2) {
            if (rootleftheight < rootrightheight) {
                int rightheight1 = 0;
                int rightheight2 = 0;

                if (root->right->right) rightheight2 = root->right->right->height;
                if (root->right->left) rightheight1 = root->right->left->height;

                if (rightheight1 > rightheight2) root = RLrotation(root);
                else  root = RRrotation(root);
            }
            else {
                int leftheight1 = 0;
                int leftheight2 = 0;
                if (root->left->right) leftheight2 = root->left->right->height;

                if (root->left->left)leftheight1 = root->left->left->height;

                if (leftheight1 > leftheight2)  root = LLrotation(root);
                else root = LRrotation(root);
            }
        }

        return root;
    }

    Treenode* insert(Treenode* root, Treenode* parent, int val) {
        if (!root) {
            root = new Treenode(val);
            root->height = 1;
            root->parent = parent;
        }
        else {
            if (root->val > val) {
                root->left = insert(root->left, root, val);
                int firstheight = 0;
                int secondheight = 0;

                if (root->left) firstheight = root->left->height;
                if (root->right) secondheight = root->right->height;

                if (abs(firstheight - secondheight) == 2) {
                    if (root->left && val < root->left->val)  root = LLrotation(root);
                    else root = LRrotation(root);
                }
            }
            else {
                root->right = insert(root->right, root, val);
                int firstheight = 0;
                int secondheight = 0;

                if (root->left)  firstheight = root->left->height;
                if (root->right)  secondheight = root->right->height;

                if (abs(firstheight - secondheight) == 2) {
                    if (root->right && val < root->right->val) root = RLrotation(root);
                    else root = RRrotation(root);
                }
            }
        }
        updateHeight(root);
        return root;
    }

    Treenode* deleteNode(Treenode* root, int val) {
        if (!root) return NULL;
        else {
            if (root->val < val) {
                root->right = deleteNode(root->right, val);
                root = Balance(root);
            }
            else if (root->val > val) {
                root->left = deleteNode(root->left, val);
                root = Balance(root);
            }
            else {   //find delete point (4 situations)
                if (root->left && !root->right) {
                    if (root->parent) {
                        if (root->parent->val < root->val)root->parent->right = root->left;
                        else root->parent->left = root->left;
                        updateHeight(root->parent);
                    }

                    root->left->parent = root->parent;
                    root->left = Balance(root->left);
                    return root->left;
                }
                else if (!root->left && root->right) {
                    if (root->parent) {
                        if (root->parent->val < root->val) root->parent->right = root->right;
                        else  root->parent->left = root->right;
                        updateHeight(root->parent);
                    }

                    root->right->parent = root->parent;
                    root->right = Balance(root->right);
                    return root->right;
                }
                else if (!root->left && !root->right) {
                    if (root->parent->val < root->val)  root->parent->right = NULL;
                    else  root->parent->left = NULL;

                    if (root->parent) updateHeight(root->parent);
                    root = NULL;
                    return NULL;
                }
                else {
                    Treenode* temp = findMax(root->left);
                    int val = temp->val;

                    root->left = deleteNode(root->left, temp->val);
                    root->val = val;
                    root = Balance(root);
                }

                Treenode* findCriticalPoint = root;

                while (findCriticalPoint->parent) {
                    findCriticalPoint = findCriticalPoint->parent;
                    findCriticalPoint = Balance(findCriticalPoint);
                }
            }

            if (root)updateHeight(root);
        }

        return root;
    }


    Treenode* findMax(Treenode* root) {
        while (root->right)root = root->right;
        return root;
    }



    string print_preorder(Treenode* root, string str) {
        string res = "";
        string left_str = "";
        string right_str = "";

        if (!root)return res;
        else {
            res = to_string(root->val)
                + '(' + print_preorder(root->left, left_str) + ')'
                + '(' + print_preorder(root->right, right_str) + ')';
        }

        return res;
    }

    string print_inorder(Treenode* root, string str) {
        string res = "";
        string left_str = "";
        string right_str = "";

        if (!root)return res;
        else {
            res = '(' + print_inorder(root->left, left_str) + ')'
                + to_string(root->val)
                + '(' + print_inorder(root->right, right_str) + ')';
        }

        return res;
    }

    string print_postorder(Treenode* root, string str) {
        string res = "";
        string left_str = "";
        string right_str = "";

        if (!root)return res;
        else {
            res = '(' + print_postorder(root->left, left_str) + ')'
                + '(' + print_postorder(root->right, right_str) + ')'
                + to_string(root->val);
        }

        return res;
    }

    void print(Treenode* root) {
        if (!root)return;
        else {
            print(root->left);
            cout << "value: " << root->val;
            cout << " height: " << root->height;
            cout << " address: " << root;
            cout << " parent: " << root->parent;
            cout << endl;
            print(root->right);
        }
        return;
    }

};


int main() {
    int N;
    cin >> N;

    AVL avl;


    //initial insertions of AVL
    while (N--) {
        int value;
        cin >> value;

        avl.root = avl.insert(avl.root, NULL, value);
    }


    char input = '\0';
    while (cin >> input) {
        switch (input) {
        case 'P': {
            int format;
            cin >> format;

            string res = "";

            switch (format) {
            case 1:
                cout << avl.print_preorder(avl.root, res) << endl;
                break;
            case 2:
                cout << avl.print_inorder(avl.root, res) << endl;
                break;
            case 3:
                cout << avl.print_postorder(avl.root, res) << endl;
                break;
            default://default
                break;
            }

            //avl.print(avl.root);

            break;
        }
        case 'I': {
            int value;
            cin >> value;

            avl.root = avl.insert(avl.root, NULL, value);
            break;
        }
        case 'D': {
            int key_value;
            cin >> key_value;

            if (avl.root->height <= 1)avl.root = NULL;
            else avl.root = avl.deleteNode(avl.root, key_value);
            break;
        }


        default://default
            break;
        }

    }


}