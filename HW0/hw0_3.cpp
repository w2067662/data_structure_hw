#include<iostream>
#include<string>
#include<vector>
using namespace std;

int mod_997(string str) {
    int temp = 0;
    for (int i = 0; i < str.length(); i++) {
        temp = temp * 10 + (str[i] - '0');
        if (temp >= 997) {
            temp %= 997;
        }
    }
    return temp;
}

string divid_by_two(string str) {
    string ans="";
    int temp = 0;
    for (int i = 0; i < str.length(); i++) {
        temp = temp * 10 + (str[i] - '0');
        if (temp >= 2) {
            ans += (temp / 2) + '0';
            temp %= 2;
        }
        else { 
            if (!str.empty())ans += '0';
        }
    }

    return ans;
}
string multiply(string num1, string num2) {
    string ans;
    vector<int> a, b, c;
    c.resize(num1.size() + num2.size() - 1);
    for (int i = num1.size() - 1; i >= 0; i--) a.push_back(num1[i] - '0');
    for (int i = num2.size() - 1; i >= 0; i--) b.push_back(num2[i] - '0');

    for (int i = 0; i < a.size(); i++) {
        for (int j = 0; j < b.size(); j++) {
            c[i + j] += a[i] * b[j];
        }
    }

    int k = 0;
    for (int i = 0; i < c.size(); i++) {
        k += c[i];
        char ch = k % 10 + '0';
        ans = ch + ans;
        k /= 10;
    }

    if (k) {
        char ch = k % 10 + '0';
        ans = ch + ans;
    }

    while (ans.size() > 1 && ans[0] == '0') ans.erase(ans.begin());
    return ans;
}


string reverse(string str) {
	char temp = '\0';
	for (int i = 0; i < str.length() / 2; i++) {
		temp = str[i];
		str[i] = str[str.length() - i - 1];
		str[str.length() - i - 1] = temp;
	}
	return str;
}

int main() {
    string n = "";
    while (cin >> n) {

		
		string m = n;
        m = reverse(m);
		for (int i = 0; i < m.length(); i++) {
			if (m[i] == '9') {
				if (i== m.length()-1)m += '1';
				m[i] = '0';
			}
			else {
				m[i] += 1;
				break;
			}
		}
        m = reverse(m);


        string ans = "";
        ans = multiply(m, n);
        ans = divid_by_two(ans);
        int answer = mod_997(ans);

        cout << answer << endl;
    }
}