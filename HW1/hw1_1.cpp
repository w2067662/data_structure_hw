#include<iostream>

using namespace std;

//for start point
//Pos (x,y)
typedef struct Pos {
    int x;
    int y;
};

//define element
const char CURR = '.';
const char WALL = '#';
const char AISLE = '*';
const char DOOR = '@';

//template for queue
template<class T>
class Queue {
private:
    int capacity;
    int front;
    int back;
    T* queue;
public:
    Queue() :capacity(5), front(0), back(0) {
        queue = new T[capacity];
    }
    void doubleCapacity() {
        T* newQueue = new T[capacity * 2];

        int j = front, temp_size = size();
        for (int i = 1; i <= temp_size; i++) {
            newQueue[i] = queue[++j % capacity];
        }

        back = size();
        front = 0;
        capacity *= 2;

        delete[] queue;
        queue = newQueue;
    }

    void push(T pos) {
        if (full()) doubleCapacity();
        back = (back + 1) % capacity;
        queue[back] = pos;
    }
    void pop() {
        if (empty())  return;
        front = (front + 1) % capacity;
    }
    bool empty() {
        return (front == back);
    }
    bool full() {
        return ((back + 1) % capacity == front);
    }
    T getFront() {
        if (empty())  return { -1,-1 };
        return queue[(front + 1) % capacity];
    }
    T getBack() {
        if (empty()) return { -1,-1 };
        return queue[back];
    }
    int size() {
        int size;
        if (front < back)   size = back - front;
        else   size = capacity - (front - back);
        return size;
    }
};


//specialize template for queue<Pos>
template<>
class Queue<Pos> {
private:
    int capacity;
    int front;
    int back;
    Pos* queue;
public:
    Queue() :capacity(5), front(0), back(0) {
        queue = new Pos[capacity];
    }

    Queue(int Cap) :capacity(Cap), front(0), back(0) {
        queue = new Pos[capacity];
    }

    void doubleCapacity() {
        Pos* newQueue = new Pos[capacity * 2];

        int j = front, temp_size = size();
        for (int i = 1; i <= temp_size; i++) {
            newQueue[i] = queue[++j % capacity];
        }

        back = size();
        front = 0;
        capacity *= 2;

        delete[] queue;
        queue = newQueue;
    }

    void push(Pos pos) {
        if (full())  doubleCapacity();
        back = (back + 1) % capacity;
        queue[back] = pos;
    }
    void pop() {
        if (empty())return;
        front = (front + 1) % capacity;
    }

    bool empty() {return (front == back); }

    bool full() {  return ((back + 1) % capacity == front);  }

    Pos getFront() {
        if (empty())return { -1,-1 };
        return queue[(front + 1) % capacity];
    }

    Pos getBack() {
        if (empty()) return { -1,-1 };
        return queue[back];
    }

    int size() {
        if (front < back)  return back - front;
        else return capacity - (front - back);
    }
};



//use BSF to search shortest route
int BSF(char** board, Pos start, int M, int N) {
    //allocate memory for visited
    bool** visited = new bool* [M];
    for (int i = 0; i < M; i++) {
        bool* visited_ptr = new bool[N];
        visited[i] = visited_ptr;
    }

    //visited initialize
    for (int j = 0; j < M; j++) {
        for (int i = 0; i < N; i++) {
            if (board[j][i] == WALL) visited[j][i] = true;
            else visited[j][i] = false;
        }
    }

    //offsets initialize
    const int offset_size = 4;
    Pos offsets[offset_size] = { {0,1},{1,0}, {0,-1},{-1,0} };

    Queue<Pos> queue(M * N);
    queue.push(start);
    visited[start.y][start.x] = true;

    int steps = -1;

    while (!queue.empty()) {

        //visualize visiting process
        /*for (int j = 0; j < M; j++) {
            for (int i = 0; i < N; i++) {
                cout << visited[j][i];
            }
            cout << endl;
        }cout << endl; cout << endl;*/

        steps++;
        int queue_size = queue.size();

        for (int j = 0; j < queue_size; j++) {

            Pos current = queue.getFront();

            if (current.x >= 0 && current.y >= 0 &&
                current.x < N && current.y < M)
            {
                if (board[current.y][current.x] == DOOR) return steps;
            }

            for (int i = 0; i < offset_size; i++) {
                int next_x = current.x + offsets[i].x;
                int next_y = current.y + offsets[i].y;
                Pos next_step = { next_x , next_y };

                if (next_x < 0 || next_y < 0 || next_x >= N ||
                    next_y >= M || board[next_y][next_x] == WALL)continue;

                if (!visited[next_y][next_x]) {
                    visited[next_y][next_x] = true;
                    queue.push(next_step);
                }


            }
            queue.pop();
        }
    }


    return -1;
}




int main() {

    int M, N;

    while (cin >> M >> N) {

        //allocate memory for board
        char** board = new char* [M];
        for (int i = 0; i < M; i++) {
            char* board_ptr = new char[N];
            board[i] = board_ptr;
        }


        //board initialize
        for (int j = 0; j < M; j++) {
            for (int i = 0; i < N; i++) {
                board[j][i] = '\0';
            }
        }

        //construct and initialize start point
        Pos start;
        start = { 0,0 };

        //get board input
        string input_line;
        int j = 0;
        while (cin >> input_line) {
            for (int i = 0; i < N; i++) {
                board[j][i] = input_line[i];

                if (board[j][i] == CURR) {
                    start = { i,j };
                }
            }

            j++;
            if (j >= M)break;
        }

        //check for board input
        /*for (int j = 0; j < M; j++) {
         for (int i = 0; i < N; i++) {
          cout<< board[j][i];
         }
         cout << endl;
        }
        cout << start.y << ";" << start.x << endl;*/

        //do BSF and print the answer
        cout << BSF(board, start, M, N) << endl;
    }


}