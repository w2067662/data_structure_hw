#include<iostream>
#include<string>

using namespace std;

//template for stack<char> && stack<string>
template<typename T>
class Stack {
private:
    T *data;
    int Size;
    int Top;
    const int Buttom = 0;

public:
    Stack() : Size(5), Top(-1){
        data = new T[Size];
    }

    Stack(int size) : Size(size), Top(-1) {
        data = new T[Size];
    }

    void doubleSize() {
        int new_size = Size * 2;
        T *new_data = new T[new_size];

        for (int i = 0; i < Size; i++) {
            new_data[i] = data[i];
        }
        delete[] data;
        this->data = new_data;
        this->Size = new_size;
    }

    bool push(T const& x) {
        if (full())doubleSize();
        int temp = Top + 1;
        data[temp] = x;
        this->Top = temp;
        return true;
    }

    bool empty() {
        return Top == -1;
    }

    T const& top() const{
        return data[Top];
    }

    bool pop() {
        if (empty())return false;
        this->Top -= 1;
        return true;
    }

    bool full() {
        return Top + 1 >= Size;
    }

    int size() {
        return Top + 1;
    }

};


void swap(char* x, char* y) {
    char tmp = *x;
    *x = *y;
    *y = tmp;
}

void reverse(char* first, char* last) {
    --last;
    while (first < last) {
        swap(first, last);
        ++first;
        --last;
    }
}

string decryption(string str) {
    string temp_char = "";
    string copy_count = "";
    Stack<char> stack;

    for (int i = 0; i < str.length();i++) {

        //5[d2[o]]
        //******^
        //stack = {'5','[','d','2','[','o'}
        if (str[i] != ']') {
            stack.push(str[i]);
        }
        else {
            //5[d2[o]]
            //****^
            //stack = {'5','[','d','2','['}
            //temp_char = "o"
            while (stack.top() != '[') {
                temp_char = stack.top() + temp_char;
                stack.pop();
            }

            //stack = {'5','[','d','2'}
            //temp_char = "o"
            stack.pop();

            //stack = {'5','[','d'}
            //temp_char = "o"
            //copy_count = "2"
            while (stack.top() >= '0' && stack.top() <= '9') {
                copy_count = stack.top() + copy_count;
                stack.pop();
                if (stack.empty()) break;
            }

            string repeat = "";

            //stack = {'5','[','d'}
            //repeat = "oo"
            for (int i = 0; i < stoi(copy_count); i++) {
                repeat += temp_char;
            }

            
            //stack = {'5','[','d','o','o'}
            for (int j = 0; j < repeat.length(); j++) {
                stack.push(repeat[j]);
            }

            temp_char = "";
            copy_count = "";

            //5[doo]
            //*****^
        }
    }

    string res = "";
    while (!stack.empty()) {
        res = stack.top() + res;
        stack.pop();
    }

    return res;
}

string swap_by_symbol(string str) {
    Stack<char> stack;
    string temp_string = "";
    string temp_char = "";
    int brace_count = 0;
    bool find_symbol = false;

    for (int i = 0; i < str.length(); i++) {
        if (str[i] != ']') {
            stack.push(str[i]);
        }
        else {
            while (stack.top() != '[' || brace_count!=0) {
                if (stack.top() == '<') find_symbol = true;
                if (stack.top() == '[') brace_count--;
                else if(stack.top() == ']')brace_count++;
                temp_char = stack.top() + temp_char;
                stack.pop();
            }

            //cout << temp_char << endl;

            if (find_symbol) {
                temp_char = swap_by_symbol(temp_char);
                find_symbol = false;
            }

            for (int j = 0; j < temp_char.length(); j++) {
                stack.push(temp_char[j]);
            }

            stack.push(']');
            temp_char = "";
        }

    }

    while (!stack.empty()) {
        temp_string = stack.top() + temp_string;
        stack.pop();
    }

    //cout << temp_string << endl;

    
    for (int i = 0; i < temp_string.length(); i++) {
        if (temp_string[i] == '<') {
            temp_string[i] = '*';
            int left = i - 1;
            int right = i + 1;

            //find left
            
            if (temp_string[left] == ']') {
                int left_brace_count = -1;
                while (temp_string[left] != '[' || left_brace_count != 0) {
                    if (temp_string[left] == '[') left_brace_count--;
                    else if (temp_string[left] == ']')left_brace_count++;
                    left--;
                }
                left--;
                while (temp_string[left] >= '0' && temp_string[left] <='9' && left > 0) {
                    left--;
                }
                if (left != 0 || (temp_string[left]>='a'&& temp_string[left]<='z'))left++;
            }

            //find right
            if (temp_string[right] >= '0' && temp_string[right] <= '9') {
                int right_brace_count = -1;
                while ((temp_string[right] != ']' || right_brace_count != 0) && right< temp_string.length()-1) {
                    if (temp_string[right] == '[') right_brace_count++;
                    else if (temp_string[right] == ']')right_brace_count--;
                    right++;
                }
            }

            reverse(&temp_string[left], &temp_string[i - 1 + 1]);
            //cout << temp_string << endl;
            reverse(&temp_string[i + 1], &temp_string[right + 1]);
            //cout << temp_string << endl;
            reverse(&temp_string[left], &temp_string[right + 1]);
            //cout << temp_string << endl;

        }
    }

    string res = "";
    for (int i = 0; i < temp_string.length(); i++) {
        if(temp_string[i] !='*')res = res + temp_string[i];
    }
    //cout <<endl << res << endl;
    return res;
}

int main() {
    string input;
    while (cin >> input) {

        cout << decryption(swap_by_symbol(input)) << endl;

    }
}