#include <iostream>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <vector>
#include <algorithm>

using namespace std;

struct edge {
    int weight;
    int u;
    int v;
    edge(int w, int u, int v) :weight(w), u(u), v(v) {}
};

class compare {
public:
    inline bool operator()(const edge& a, const edge& b) {
        if (a.weight < b.weight)return true;
        else if (a.weight == b.weight) {
            if (a.u < b.u)return true;
            else if (a.u == b.u) {
                if (a.v <= b.v)return true;
                else return false;
            }
            else return false;
        }
        else return false;
    }
};

struct MST {
    struct forest {
        unordered_map<int, int> parent, rank;

        forest(unordered_set<int> nodes) {
            for (const auto& node: nodes) {
                rank.insert({ node, 0 });
                parent.insert({ node,  node });
            }
        }

        int find(int u) {
            if (u != parent[u]) parent[u] = find(parent[u]);
            return parent[u];
        }

        void merge(int u, int v) {
            u = find(u);
            v = find(v);
            if (rank[u] > rank[v])parent[v] = u;
            else  parent[u] = v;

            if (rank[u] == rank[v]) rank[v]++;
        }
    };

private:
    unordered_set<int> nodes;
    vector<edge>  edges;
    int nodeAmount;
    int edgeAmount;

public:
    MST(int M, int N) {
        this->nodeAmount = M;
        this->edgeAmount = N;
    }

    void insertEdge(int u, int v, int w) {
        if (u <= v) {
            edge temp(w, u, v);
            edges.push_back(temp);
        }
        else {
            edge temp(w, v, u);
            edges.push_back(temp);
        }
        nodes.insert(u);
        nodes.insert(v);
    }

    void print(void) {
        sort(edges.begin(), edges.end(), compare()); //sort edges by {1.weight 2.u 3.v}

        //for (vector< edge >::iterator it = edges.begin(); it != edges.end(); it++) {
        //    cout << it->weight << " ";
        //    cout << it->u << ";" << it->v << endl;
        //}

        forest forest(nodes);

        for (vector< edge >::iterator it = edges.begin(); it != edges.end(); it++) {
            int forest_u = forest.find(it->u);
            int forest_v = forest.find(it->v);
            //cout << "find " << forest_u << ";" << forest_v << endl;

            if (forest_u != forest_v) {
                cout << it->u << " " << it->v;
                cout << " " << it->weight << endl;

                forest.merge(forest_u, forest_v);
            }
        }
    }
};






int main() {
    int M, N;
    while (cin >> M) {
        cin >> N;
        MST mst(M, N);

        int u, v, weight;
        for (int i = 0; i < N; i++) {
            cin >> u; cin >> v; cin >> weight;
            mst.insertEdge(u, v, weight);
        }

        mst.print();

    }
}
