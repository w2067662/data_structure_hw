#include <iostream>
#include <unordered_map>
#include <map>
#include <stack>
#include <unordered_set>
#include <vector>

using namespace std;

class WeightedGraph {
private:
    unordered_map<int, unordered_map<int, int>> adjacency_list;//{u, {v, weight}}
    int start_point;

public:
    WeightedGraph() {}

    void setStartPoint(int start) {
        start_point = start;
    }

    void insertEdge(int u, int v, int weight) {
        if (adjacency_list.find(u) != adjacency_list.end()) {
            adjacency_list[u].insert({ v, weight });
        }
        else {
            unordered_map<int, int> temp;
            temp.insert({ v, weight });
            adjacency_list.insert({ u, temp });
        }
    }

    void printGraph() {
        for (const auto& j : adjacency_list) {
            cout << "node of index " << j.first << ": " << j.first;
            for (const auto& i : adjacency_list[j.first]) {
                int it = i.first;
                int weight = i.second;
                cout << "->" << it << "(" << weight << ")";
            }
            cout << endl;
        }
    }

    void PrintAscendingDistanceNodes(void) {
        map<int, vector<int>> ans;// {min Distance, node value[0~n]}
        stack<int> stack;
        unordered_set <int> reachableNodes;
        unordered_set <int> visited;

        stack.push(start_point);
        reachableNodes.insert(start_point);
        visited.insert(start_point);

        //find and store reachable nodes into set
        while (!stack.empty()) {
            int next = stack.top();
            //cout << "next: " << next << endl;
            stack.pop();
            for (const auto& it : adjacency_list[next]) {
                if (visited.find(it.first) == visited.end()) {
                    stack.push(it.first);
                    reachableNodes.insert(it.first);
                    visited.insert(it.first);
                }
            }
        }

        //print set
        //for (const auto& it : reachableNodes) {
        //    cout << it << " ";
        //}
        //cout << endl;

        //for each reachable Node, find min distance
        for (const auto& node : reachableNodes) {
            visited.clear();
            while (!stack.empty()) stack.pop();
            int minDis = 2147483647;

            visited.insert(start_point);
            stack.push(start_point);

            DFS(node, start_point, 0, &visited, stack, &minDis);

            if (ans.find(minDis) != ans.end()) {
                ans[minDis].push_back(node);
            }
            else {
                vector<int> temp;
                temp.push_back(node);
                ans.insert({ minDis, temp });
            }
        }

        //print answers
        for (const auto& it : ans) {
            for (int i = 0; i < it.second.size(); i++) {
                cout << it.second[i] << " " << it.first << endl;
            }
        }

        return;
    }

    void DFS(int key, int last, int Distance, unordered_set<int>* visited, stack<int> stack, int* minDis) {
        int curr = stack.top();
        stack.pop();

        if (curr != start_point) Distance += adjacency_list[last][curr];//add distance
        //cout << "find node: " << key;
        //cout << " last-curr: " << last << "->" << curr << " currentDis: " << Distance << endl;

        

        if (curr == key ) {
            if(*minDis > Distance)*minDis = Distance;
            return;
        }
        if (*minDis <= Distance)return;

        //bool hasNext = false;
        for (const auto& next : adjacency_list[curr]) {
            if (visited->find(next.first) == visited->end()) {//not visited
                //hasNext = true;
                stack.push(next.first);
                visited->insert(next.first);
                DFS(key, curr, Distance, visited, stack, minDis);
                visited->erase(next.first);
            }
        }

        return;
    }

};

int main() {

    int M, N;
    while (cin >> M) {
        cin >> N;
        WeightedGraph graph;

        int u, v, weight;
        for (int i = 0; i < N; i++) {
            cin >> u; cin >> v; cin >> weight;
            graph.insertEdge(u, v, weight);
        }

        int start_point;
        cin >> start_point;
        graph.setStartPoint(start_point);

        //graph.printGraph();

        graph.PrintAscendingDistanceNodes();
    }
}
