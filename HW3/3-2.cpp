#include<iostream>
using namespace std;

template <typename T> 
class container{
    T* arr;
    int capacity;
    int current;
    int K;
    int N;

public:
    container() {
        arr = new T[1];
        capacity = 1;
        current = 0;
        K = 0;
    }

    container(int size) {
        arr = new T[size];
        capacity = size;
        current = 0;
        K = 0;
    }

    void push(T data) {
        if (current == capacity) {
            T* temp = new T[2 * capacity];
            for (int i = 0; i < capacity; i++) {
                temp[i] = arr[i];
            }

            delete[] arr;
            capacity *= 2;
            arr = temp;
        }

        arr[current] = data;
        current++;
    }

    void push(T data, int index) {
        if (index == capacity) push(data);
           
        else arr[index] = data;
    }

    void setK(int val) {
        K = val;
    }

    void setN(int val) {
        N = val;
    }

    T get(int index) {
        if (index < current && index >= 0)return arr[index];   
    }

    T operator[] (int index) {
        if (index < current && index >= 0)return arr[index];
    }

    void pop() { current--; }

    int size() { return current; }

    int getcapacity() { return capacity; }

    void print() {
        for (int i = 0; i < current; i++) {
            cout << arr[i] << " ";
        }
        cout << endl;
    }

    void append(container<T> list) {
        int size = current + list.current + 1;
        T* temp = new T[size + 1];

        for (int i = 0; i < current; i++) {
            temp[i] = arr[i];
        }
        for (int i = 0 ; i < list.current; i++) {
            temp[i + current] = list.arr[i];
        }

        this->current = size;
        this->capacity = size +1 ;

        delete[] arr;
        arr = temp;
    }

    int binary_search(int start, int end, int key) {
        int ans = -1;

        int mid;
        while (start <= end) {
            mid = start + (end - start) / 2; 
            if (arr[mid] < key)
                start = mid + 1;
            else if (arr[mid] > key)
                end = mid - 1;
            else {            
                ans = mid;
                break;
            }
        }
        return ans;     
    }

    int findIndex(int key) {
        int index = binary_search(0, current-1, key);

        if (index == -1)return index;
        else if (index >= (N - K)) return index - (N-K);
        else return index + K;
    }
};



int main() {
    container<int> list1, list2;
    container<int>* list_ptr = &list2;
	int N, M;
	int k = 0;
	int max = 0;
	int input_number;


	while (cin >> N) {
        bool tag = true;
		for (int i = 0; i < N; i++) {
			cin >> input_number;
			if (input_number >= max) max = input_number;
			else {
                if (tag) {
                    k = i;
                    //cout << k << endl;
                    list_ptr = &list1;
                    tag = false;
                }
			}
            list_ptr->push(input_number);
		}
        list1.append(list2);
        list1.setK(k);
        list1.setN(N);
        

		/*for (int i = 0; i < N; i++) {
			cout << list1[i] << endl;
		}*/

		cin >> M;

		for (int i = 0; i < M; i++) {
			cin >> input_number;
			cout << list1.findIndex(input_number) << endl;
		}

        break;
	}
    return 0;

}