#include<iostream>
#include<string>
using namespace std;

typedef enum Color {
    RED = 0,
    BLACK = 1,
};

struct Treenode {
    int val;
    Color color;
    Treenode* left, * right, * parent;
    Treenode() : val(0), left(NULL), right(NULL), color(RED), parent(NULL) {};
    Treenode(int val) : val(val), left(NULL), right(NULL), color(RED), parent(NULL) {};
};



class RBT {
public:
    Treenode* root;
    Treenode* NIL;
public:
    RBT() {
        NIL = new Treenode();
        NIL->color = BLACK;
        root = NULL;
    }

    Treenode* grandparent(Treenode* node) {
        if (!node->parent)return NULL;
        return node->parent->parent;
    }

    Treenode* uncle(Treenode* node) {
        if (!grandparent(node))return NULL;
        if (node->parent == grandparent(node)->left) return grandparent(node)->right;
        else return grandparent(node)->left;
    }

    void swapColor(Color left_color, Color right_color) {
        Color temp = left_color;
        left_color = right_color;
        right_color = temp;
    }

    void rotate_right(Treenode* node) {
        if (!node->parent) {
            root = node;
            return;
        }
        Treenode* grandParent = grandparent(node);
        Treenode* father = node->parent;
        Treenode* temp = node->right;

        father->left = temp;

        if (temp != NIL)temp->parent = father;
        node->right = father;
        father->parent = node;

        if (root == father) root = node;
        node->parent = grandParent;

        if (grandParent) {
            if (grandParent->left == father) grandParent->left = node;
            else grandParent->right = node;
        }

    }

    void rotate_left(Treenode* node) {
        if (!node->parent) {
            root = node;
            return;
        }
        Treenode* grandParent = grandparent(node);
        Treenode* father = node->parent;
        Treenode* temp = node->left;

        father->right = temp;

        if (temp != NIL) temp->parent = father;
        node->left = father;
        father->parent = node;

        if (root == father) root = node;
        node->parent = grandParent;

        if (grandParent) {
            if (grandParent->left == father)grandParent->left = node;
            else  grandParent->right = node;
        }
    }

    void checkColorChange(Treenode* node) {
        if (!node || node == NIL)return;
        else {
            bool twoRedChildren = false;
            if (node->left != NIL && node->right != NIL) {
                if(node->left->color == RED && node->right->color == RED)twoRedChildren = true;
            }
            if (twoRedChildren) {
                node->color = RED;
                node->left->color = node->right->color = BLACK;
                //rotation after colorchange
                doRotation(node);
            }
        }
        return;
    }

    void doRotation(Treenode* node) {
        if (!node->parent) {
            root = node;
            return;
        }

        if (node->parent->color == RED) {
            //LRrotation
            if (node->parent->right == node && grandparent(node)->left == node->parent) {
                rotate_left(node);
                node->color = BLACK;
                node->parent->color = RED;
                rotate_right(node);
            }
            //RLrotation
            else if (node->parent->left == node && grandparent(node)->right == node->parent) {
                rotate_right(node);
                node->color = BLACK;
                node->parent->color = RED;
                rotate_left(node);
            }
            //LLrotation
            else if (node->parent->left == node && grandparent(node)->left == node->parent) {
                node->parent->color = BLACK;
                grandparent(node)->color = RED;
                rotate_right(node->parent);
            }
            //RRrotation
            else if (node->parent->right == node && grandparent(node)->right == node->parent) {
                node->parent->color = BLACK;
                grandparent(node)->color = RED;
                rotate_left(node->parent);
            }
        }
    }

    void insertRBT(Treenode* node, int val) {
        //colorchange check within rotation
        checkColorChange(node);

        if (val <= node->val) {
            if (node->left != NIL) insertRBT(node->left, val);
            //insert
            else {
                Treenode* temp = new Treenode(val);
                temp->left = temp->right = NIL;
                temp->parent = node;
                node->left = temp;
                //rotation after insertion
                doRotation(temp);
            }
        }
        else {
            if (node->right != NIL)insertRBT(node->right, val);
            //insert
            else {
                Treenode* temp = new Treenode(val);
                temp->left = temp->right = NIL;
                temp->parent = node;
                node->right = temp;
                //rotation after insertion
                doRotation(temp);
            }
        }
    }

    void insert(int val) {
        if (!root) {
            root = new Treenode(val);
            root->color = BLACK;
            root->left = root->right = NIL;
        }
        else {
            insertRBT(root, val);
            root->color = BLACK;
        }
    }

    string print_preorder(Treenode* root, string str) {
        string res = "";
        string left_str = "";
        string right_str = "";
        string color = "";

        if (!root)return res;
        else if (root != NIL) {
            color = (root->color == BLACK) ? color = "_b" : "_r";

            res = to_string(root->val) + color
                + '(' + print_preorder(root->left, left_str) + ')'
                + '(' + print_preorder(root->right, right_str) + ')';
        }

        return res;
    }

    void print(Treenode* root) {
        if (!root)return;
        else {
            print(root->left);
            cout << "val: " << root->val;
            cout << " color: " << root->color;
            cout << " address: " << root;
            cout << " parent: " << root->parent;
            cout << endl;
            print(root->right);
        }
        return;
    }

};


int main() {

    RBT rbt;
    char input = '\0';
    while (cin >> input) {
        switch (input) {
        case 'P': {
            string res = "";
            cout << rbt.print_preorder(rbt.root, res) << endl;

            break;
        }
        case 'I': {
            int val;
            cin >> val;

            rbt.insert(val);

            break;
        }
        default://default
            break;
        }
    }

}